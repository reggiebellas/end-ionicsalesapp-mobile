export interface IMtdDealerStats {
    product: string;
    sales: number;
    dailyAverage: number;
    monthEstimate: number;
}