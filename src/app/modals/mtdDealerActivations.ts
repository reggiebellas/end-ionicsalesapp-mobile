export interface IMtdDealerActivations {
    product: string;
    sales: number;
    dailyAverage: number;
    monthEstimate: number;
}