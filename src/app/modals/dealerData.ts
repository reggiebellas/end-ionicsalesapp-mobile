import { IYesterdaysDealerStats } from "./yesterdaysDealerStats";
import { IMtdDealerStats } from "./mtdDealerStats";
import { IMtdDealerActivations } from "./mtdDealerActivations";

export interface IDealersData {
    yesterdaysDealerStats: Array<IYesterdaysDealerStats>;
    mtdDealerStats: Array<IMtdDealerStats>;
    mtdDealerActivations: Array<IMtdDealerActivations>;
}