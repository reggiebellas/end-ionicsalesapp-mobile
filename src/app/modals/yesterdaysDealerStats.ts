export interface IYesterdaysDealerStats {
    product: string;
    sales: number;
}