import { Injectable } from '@angular/core';
import { CommonHttpMethodsService } from './common-http-methods.service';
import { environment } from '../../environments/environment';
import { Observable } from 'rx';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private _httpMethodsService: CommonHttpMethodsService) { }

  login(data) {
    return this._httpMethodsService.post(environment.securityTokenApiUrl, data);
  }

}
