import { TestBed, inject } from '@angular/core/testing';

import { DealersDataService } from './dealer-data.service';

describe('SalesDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DealersDataService]
    });
  });

  it('should be created', inject([DealersDataService], (service: DealersDataService) => {
    expect(service).toBeTruthy();
  }));
});
