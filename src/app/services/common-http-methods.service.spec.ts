import { TestBed, inject } from '@angular/core/testing';

import { CommonHttpMethodsService } from './common-http-methods.service';

describe('CommonHttpMethodsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CommonHttpMethodsService]
    });
  });

  it('should be created', inject([CommonHttpMethodsService], (service: CommonHttpMethodsService) => {
    expect(service).toBeTruthy();
  }));
});
