import { TestBed, inject } from '@angular/core/testing';

import { SetHeaderInterceptor } from './token-interceptor.service';

describe('TokenInterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SetHeaderInterceptor]
    });
  });

  it('should be created', inject([SetHeaderInterceptor], (service: SetHeaderInterceptor) => {
    expect(service).toBeTruthy();
  }));
});
