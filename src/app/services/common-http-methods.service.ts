import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommonHttpMethodsService {

  constructor(private _http: HttpClient) { }

  get(url: string): Observable<any> {
    return this._http.get<any>(url)
      .pipe(
        map(data => data),
        catchError(error => this.handleError(error))
      );
  }

  post(url: string, body: any): Observable<any>  {
    const requestBody = JSON.stringify(body);
    return this._http.post(url, requestBody).pipe(
      map(data => data),
      catchError(error => this.handleError(error))
    );
  }

  private handleError(error): Observable<any> { // TODO move to error handling class
    console.log(`error reponse ${error}`);
    return error;
 }
}
