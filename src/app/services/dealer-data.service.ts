import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonHttpMethodsService } from './common-http-methods.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DealersDataService {

  constructor(private _httpMethodsService: CommonHttpMethodsService) { }

  getDealerStats(): Observable<any> {
    return this._httpMethodsService.get(environment.dealerStatsUrl);
  }

  getClaimsData(): Observable<any> {
    return this._httpMethodsService.get(environment.claimsUrl);
  }

}
