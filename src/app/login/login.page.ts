import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { FormBuilder, Validators, FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private _router: Router,
    private _authService: AuthService,
    public _fb: FormBuilder) { }
    
    public userFrom: FormGroup;
    public userForm = this._fb.group({
    'userName': ['', [Validators.required, Validators.minLength(1)]],
    'password': ['', [Validators.required, Validators.minLength(1)]]
  });

  ngOnInit() {}
   
  login(): void {
  /*  if (!this.userForm.touched && !this.userForm.valid) {
      alert(`Email: ${this.userForm.value.email} Password: ${this.userForm.value.password}`);
    } else {
      this._authService.login(this.userForm.value)
        .subscribe(data => {
          if (data.token) {
            this.navToHomeDealerPage();
          } else {
            // add logic for some error or no token
          }
        }); */
  //  }

  this.navToHomeDealerPage();
  }

  navToHomeDealerPage(): void {
    this._router.navigate(['/sales']);
  }
}
