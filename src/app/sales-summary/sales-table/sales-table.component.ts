import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sales-table',
  templateUrl: './sales-table.component.html',
  styleUrls: ['./sales-table.component.css']
})
export class SalesTableComponent implements OnInit {

  @Input() yesterdaysDealerStats$: any;
  @Input() mtdDealerActivations$: any;
  @Input() mtdDealerStats$: any;

  constructor() { }

  ngOnInit() {
  }

}
