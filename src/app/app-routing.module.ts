import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SalesSummaryComponent } from './sales-summary/sales-summary.component';

const routes: Routes = [
  { path: '', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule'},
  { path: 'sales', component: SalesSummaryComponent  }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
