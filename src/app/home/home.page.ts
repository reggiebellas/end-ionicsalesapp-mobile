import { Component, OnInit } from '@angular/core';
import { DealersDataService } from '../services/dealer-data.service';
import { Observable } from '../../../node_modules/rxjs/internal/Observable';
import { interval } from 'rxjs';
import { environment } from '../../environments/environment';
import { IYesterdaysDealerStats } from '../modals/yesterdaysDealerStats';
import { map, shareReplay } from '../../../node_modules/rxjs/operators';
import { IMtdDealerActivations } from '../modals/mtdDealerActivations';
import { IMtdDealerStats } from '../modals/mtdDealerStats';
import { IDealersData } from '../modals/dealerData';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {

  constructor(private _dealersDataSearvice: DealersDataService) { }

  private dealersData$: Observable<any>;
  public yesterdaysDealerStats$: Observable<IYesterdaysDealerStats>;
  public mtdDealerActivations$: Observable<IMtdDealerActivations>;
  public mtdDealerStats$: Observable<IMtdDealerStats>;
  public todayDate: Date;
  public yesterDayDate: any;

  ngOnInit(): void {
    this.todayDate = new Date();
    this.yesterDayDate = this.getYesterdayDate();
    this.dealersData$ = this.loadDealersData().pipe(
      map(data => data),
      shareReplay() // creates one subscription from other observers
    );

    // split observables reactive design only one nextwork call
    // since shared subscription on the dealersData$ observable

    // yesterday dealer stats
    this.yesterdaysDealerStats$ = this.dealersData$.pipe(
      map(data => data.yesterdaysDealerStats)
    );
    // mtd dealer activations
    this.mtdDealerActivations$ = this.dealersData$.pipe(
      map(data => data.mtdDealerActivations)
    );
    // mtd dealer stats
    this.mtdDealerStats$ = this.dealersData$.pipe(
      map(data => data.mtdDealerActivations)
    );
    // start interval to reload data from azure
    this.reloadData();
  }

   getYesterdayDate(): any {
    const date = new Date();
    return date.setDate(date.getDate() - 1);
  }

  reloadData(): void {
    const reloadData = interval(environment.intervalTimer);
    reloadData.subscribe(() => {
      this.dealersData$ = this._dealersDataSearvice.getDealerStats();
    });
  }

  loadDealersData(): Observable<any> {
    return this._dealersDataSearvice.getDealerStats();
  }

}
