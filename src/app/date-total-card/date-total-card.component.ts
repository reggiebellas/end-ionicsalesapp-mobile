import { Component, OnInit, Input } from '@angular/core';
import * as _ from 'lodash';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-date-total-card',
  templateUrl: './date-total-card.component.html',
  styleUrls: ['./date-total-card.component.css']
})
export class DateTotalCardComponent implements OnInit {

  @Input() setDateField: Date;
  @Input() textLabel: string;
  @Input() salesData: Observable<Array<any>>;

  public salesTotal: any;

  constructor() { }

  ngOnInit() {
  console.log('here');
  console.log(this.salesData);
  this.sumSalesTotal();
  }

  getYesterDayDate(): any {
    const todayDate = new Date();
    return todayDate.setDate(todayDate.getDate() - 1);
  }

  sumSalesTotal() {
   this.salesData.subscribe(data =>  this.salesTotal =  _.sumBy(data, 'sales'));
  }

}
