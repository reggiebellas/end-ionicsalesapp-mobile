import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DateTotalCardComponent } from './date-total-card.component';

describe('DateTotalCardComponent', () => {
  let component: DateTotalCardComponent;
  let fixture: ComponentFixture<DateTotalCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DateTotalCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DateTotalCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
