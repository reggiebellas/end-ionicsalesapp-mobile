export const environment = {
  production: true,
  dealerStatsUrl: 'http://dealerstats.azurewebsites.net/api/dealerstats',
  claimsUrl: 'http://dealerstats.azurewebsites.net/api/claim/claimsummary',
  securityTokenApiUrl: 'https://endurance-identitywac-dev.azurewebsites.net/api/Token/Generate',
  intervalTimer: 3600000, // milliseconds equal 1 hr
  clientId: 'ZWtAYSopOTNsZDsvZA=='
};
